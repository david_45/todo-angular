import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/throw';


@Injectable()
export class TodoService {

  private headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});

  constructor(private http: Http) {
  }

  getToDo() {
    return this.http.get(
      'http://todo.dev/api/gettodo'
    )
      .map(
        (response: Response) => {
          const data = response.json();
          return data;
        });
  }

  postToDoCreate(title, status) {

    return this.http.post('http://todo.dev/api/createtodo', {
      title: title,
      status: status
    }, {headers: this.headers})
      .map((resp: Response) => {
        const data = resp.json();
        return data;
      });
  }

  UpdateStatusToDo(status, id) {
    return this.http.post('http://todo.dev/api/updatetodo', {
      status: status,
      id: id
    }, {headers: this.headers})
      .map((resp: Response) => {
        const data = resp.json();
        return data;
      });
  }

  DeleteToDo(id) {
    return this.http.post('http://todo.dev/api/deletetodo', {
      status: status,
      id: id
    }, {headers: this.headers})
      .map((resp: Response) => {
        const data = resp.json();
        return data;
      });
  }
}
