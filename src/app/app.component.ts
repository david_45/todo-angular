import {Component, OnInit} from '@angular/core';
import {TodoService} from "./todo.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoService]
})
export class AppComponent implements OnInit {
  Allchange = true;
  Activechange = false;
  Completedchange = false;
  ToDoTitle = '';
  AllToDoList = [];
  ToDoId = [];

  constructor(private httpToDo: TodoService) {
  }


  ngOnInit() {
    this.httpToDo.getToDo()
      .subscribe(
        (data) => {
          this.AllToDoList = data.todo;

        });
  }

  todoAll() {
    this.Allchange = true;
    this.Activechange = false;
    this.Completedchange = false;
  }

  todoActive() {
    this.Allchange = false;
    this.Activechange = true;
    this.Completedchange = false;

  }

  todoCompleted() {
    this.Allchange = false;
    this.Activechange = false;
    this.Completedchange = true;
  }

  CreateToDo() {
    if (this.ToDoTitle !== '') {

      const status = 0;
      this.httpToDo.postToDoCreate(this.ToDoTitle, status)
        .subscribe((data) => {
          this.AllToDoList = data.todo;
        });
    }
  }

  ToDoChange(id, status) {
    if (status === 0) {
      this.ToDoId.push(id);
    }
    status = status === 0 ? 1 : 0;

    this.httpToDo.UpdateStatusToDo(status, id)
      .subscribe((data) => {
        this.AllToDoList = data.todo;
      });
    console.log(this.ToDoId);
  }

  DeleteToDo(id) {
    this.httpToDo.DeleteToDo(id)
      .subscribe((data) => {
        this.AllToDoList = data.todo;
      });
  }

}
